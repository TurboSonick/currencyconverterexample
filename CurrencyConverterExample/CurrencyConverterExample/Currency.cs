﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverterExample
{
    class Currency
    {
        private string name;
        private string acronym;
        private string country;
        private string flag;
        private string values;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Acronym
        {
            get
            {
                return acronym;
            }
            set
            {
                acronym = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public string Flag
        {
            get
            {
                return flag;
            }
            set
            {
                flag = value;
            }
        }

        public string Value
        {
            get
            {
                return values;
            }
            set
            {
                values = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
