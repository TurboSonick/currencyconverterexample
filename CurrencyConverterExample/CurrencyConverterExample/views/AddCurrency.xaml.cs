﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CurrencyConverterExample.views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddCurrency : ContentPage
    {
        public AddCurrency()
        {
            InitializeComponent();
        }

        async private void ImageButton_Clicked(object sender, EventArgs e)
        {
            await Navigation?.PopAsync();
        }
    }
}